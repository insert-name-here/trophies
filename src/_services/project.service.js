import config from 'config';
import { authHeader } from '../_helpers';

export const projectService = {
    getAll,
    getOneProject,
    getTrophies,
    refreshOneProject,
    authHeader,
    claimProject,
    postRepository,
    postTokenRequest,
    deleteRepository,
    createProject,
    redeemToken,
    postFunds
};

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/projects`, requestOptions).then(handleResponse);
}

function getOneProject(projectId) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${config.apiUrl}/projects/${projectId}`, requestOptions).then(handleResponse);
}

function getTrophies(trophies) {
    let projectId = localStorage.getItem('projectId');
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${config.apiUrl}/projects/${projectId}/trophies`, requestOptions).then(handleResponse);
}

function refreshOneProject(projectId) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${config.apiUrl}/${projectId}`, requestOptions).then(handleResponse);
}

function postRepository(repositoryUrl) {
    let partialHeader = { 'Content-Type': 'application/json'}
    let projectId = localStorage.getItem('projectId');

    const requestOptions = {
        method: 'POST',
        headers: jsonConcat(partialHeader, authHeader()),
        body: JSON.stringify({url: repositoryUrl})
    };
    console.log(fetch(`${config.apiUrl}/projects/${projectId}/repositories`, requestOptions));
    return fetch(`${config.apiUrl}/projects/${projectId}/repositories`, requestOptions).then(handleResponse);

}

function postFunds(funds, source) {
    let partialHeader = { 'Content-Type': 'application/json'}
    let projectId = localStorage.getItem('projectId');

    const requestOptions = {
        method: 'POST',
        headers: jsonConcat(partialHeader, authHeader()),
        body: JSON.stringify({"funds": parseInt(funds),
                               "source": source})
    };
    console.log(requestOptions.body);
    return fetch(`${config.apiUrl}/projects/${projectId}/funds`, requestOptions).then(handleResponse);

}

function postTokenRequest(emailAddress, numTrophies, justification, apiKey) {
    let partialHeader = { 'Content-Type': 'application/json',
                          'X-Api-Key': apiKey}
    let projectId = localStorage.getItem('projectId');
    console.log(apiKey);
    const requestOptions = {
        method: 'POST',
        headers: jsonConcat(partialHeader, authHeader()),
        body: JSON.stringify({"author": emailAddress,
                              "trophies": parseInt(numTrophies),
                              "justification": justification})
    };
    console.log(requestOptions.body);
    return fetch(`${config.apiUrl}/projects/${projectId}/trophies`, requestOptions).then(handleResponse);

}

function jsonConcat(o1, o2) {
 for (var key in o2) {
  o1[key] = o2[key];
 }
 return o1;
}

function deleteRepository(repositoryId) {
    let projectId = localStorage.getItem('projectId');
    //let repositoryId = localStorage.getItem('repositoryId');

     const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };
    return fetch(`${config.apiUrl}/projects/${projectId}/repositories/${localStorage.getItem('repositoryId')}`, requestOptions).then(handleResponse);   
}
function createProject(owners, name, currency) {
    var parsedOwners = owners.split(',');
    console.log(parsedOwners);
    const requestOptions = {
        method: 'POST',
        headers: jsonConcat(authHeader()),
        body: JSON.stringify({"owners": parsedOwners,
                              "name": name,
                              "currency": currency})
    };
    console.log(requestOptions.body);
    return fetch(`${config.apiUrl}/projects/`, requestOptions).then(handleResponse);    
}

function claimProject(){
    let projectId = localStorage.getItem('projectId');

    const requestOptions = {
        method: 'POST',
        headers: authHeader()
    };
    return fetch(`${config.apiUrl}/projects/${projectId}/claim`, requestOptions).then(handleResponse);    
};

function redeemToken( tokenIdSelected, depositAccount ){
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify({"trophies": [tokenIdSelected],
                              "depositAccount": depositAccount})
    };
    console.log(requestOptions.body);
    return fetch(`${config.apiUrl}/trophies/redeem`, requestOptions).then(handleResponse);    
};

authHeader()


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}