import Vue from 'vue';
import Router from 'vue-router';

import HomePage from 'home/HomePage.vue'
import ProjectPage from 'home/ProjectPage.vue'
import LoginPage from '../login/LoginPage.vue'
import RegisterPage from '../register/RegisterPage.vue'
import ConfirmRegistration from '../register/ConfirmRegistration.vue'
import TrophiesPage from 'home/TrophiesPage.vue'

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', component: HomePage, props: true  },
    { path: '/login', component: LoginPage },
    { path: '/register', name: 'RegisterPage', component: RegisterPage, props: true },
    { path: '/register/confirm', name: 'ConfirmRegistration', component: ConfirmRegistration, props: true },
    { path: '/project/', name: "ProjectPage", component: ProjectPage, props: true  },
    { path: '/project/', name: "TrophiesPage", component: TrophiesPage, props: true  },

    // otherwise redirect to home
    { path: '*', redirect: '/' }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login', '/register', '/register/confirm', '/'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('Trophies/user');

  if (authRequired && loggedIn==null) {

    return next('/login');
  }


  next();
})