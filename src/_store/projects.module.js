import { projectService } from '../_services';
import { router } from '../_helpers';

const state = {
    all: {},
    projectId: {},
    repositoryId: {},
    isHidden: false
};

const actions = {
    getAll({ commit }) {
        commit('getAllRequest');

        projectService.getAll()
            .then(
                projects => commit('getAllRequest', projects),
                error => commit('getAllFailure', error)
            );
    },
    getOneProject({ commit },  projectId ) {
        commit('getAllRequest');

        projectService.getOneProject( projectId )
            .then(
                response => {
                    commit('getOnSuccess', response);
                    router.push({
                                name: 'ProjectPage',
                                params: {projectId:localStorage.getItem('projectId')},
                                props: {items: response
                            }
                        },
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                    commit('Failure', error);
                } );
                }
            );
    },
    getTrophies({ commit },  trophies ) {
        commit('getAllRequest');

        projectService.getTrophies( trophies )
            .then(
                response => {
                    console.log(response);
                    commit('getOnSuccess', response);
                    router.push({
                                name: 'TrophiesPage',
                                params: {projectId:localStorage.getItem('projectId')},
                                props: {items: response
                            }
                        },
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                    commit('Failure', error);
                } );
                }
            );
    },
    refreshOneProject({ commit },  projectId ) {
        commit('getAllRequest');
        projectService.getOneProject( projectId )
            .then(
                response => {
                    commit('getOnSuccess', response);
                    router.push({ 
                                name: 'ProjectPage',
                                params: {projectId:localStorage.getItem('projectId')},
                                props: {items: response},
                        },
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                    commit('Failure', error);
                } );
                }
            );
    },
    authorizeHeader({ commit }){
        authHeader();
    },
    claim({ commit }) {
        commit('getAllRequest');

        projectService.claimProject()
            .then(
                response => {
                    confirm("You have successfully claimed this project");
                    router.push({
                                name: 'ProjectPage',
                                params: {projectId:localStorage.getItem('projectId')},
                                props: {items: response}},
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                        console.log("error");
                        commit('Failure', error);
                    } );
                }
            );
    },
    deleteRepository({ commit },  repositoryUrl ) {
        commit('getAllRequest');
        projectService.deleteRepository( repositoryUrl )
            .then(
                response => {
                    console.log(response);
                    router.push({
                                name: 'ProjectPage',
                                params: {   
                                            projectId:localStorage.getItem('projectId'),
                                            repositoryId: localStorage.getItem('repositoryId')
                                        },
                                props: {items: response}},
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                        location.reload(true);
                        commit('Failure', error);
                    } );
                }
            );        
    },
    updateRepository({ commit },  repositoryUrl ) {
        commit('getAllRequest');

        projectService.postRepository(repositoryUrl)
            .then(
                response => {
                    router.push({
                                name: 'ProjectPage',
                                props: {items: response}},
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                        location.reload();
                        commit('Failure', error);
                    } );
                }
            );
    },
    addFunds({ commit },  { funds, source} ) {
        commit('getAllRequest');

        projectService.postFunds(funds, source)
            .then(
                response => {
                    console.log(response);
                    router.push({
                                name: 'ProjectPage',
                                props: {items: response}},
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                        location.reload();
                        commit('Failure', error);
                    } );
                }
            );
    },
    postProject({ commit },  { owners, name, currency } ) {
        commit('getAllRequest');

        projectService.createProject(owners, name, currency)
            .then(
                response => {
                    router.push({
                                name: 'HomePage',
                                props: {items: response}},
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                        console.log("error");
                        commit('Failure', error);
                    } );
                }
            );
    },
    redeemAToken({ commit },  { tokenIdSelected, depositAccount } ) {
        commit('getAllRequest');

        projectService.redeemToken(tokenIdSelected, depositAccount )
            .then(
                response => {
                    console.log(response);
                    confirm("Congratulations!!! Your money is on its way :D")
                    router.push({
                                name: 'TrophiesPage',
                                props: {items: response}},
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                        console.log("error");
                        commit('Failure', error);
                    } );
                }
            );
    },
    tokenRequest({ commit },  { emailAddress, numTrophies, justification, apiKey }  ) {
        commit('getAllRequest');

        projectService.postTokenRequest(emailAddress, numTrophies, justification, apiKey)
            .then(
                response => {
                    confirm("Trophies issued successfully");
                    router.push({
                                name: 'ProjectPage',
                                props: {items: response}},
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                    })
                   },
                error => {
                        console.log("error");
                        commit('Failure', error);
                    } );
                }
            );
    }
};

const mutations = {
    getAllRequest(state, projects) {
        state.all = { items: projects,
                        loading: true};
        state.projectId = {};
        state.repositoryId = {};
    },
    getOneSuccess(state, projects) {
        state.all = { items: projects };
        state.projectId = { items: projects.projectId };
    },
    getOnSuccess(state, projects) {
        state.all = { items: projects };
        state.isHidden = false
    },
    deleteRequest(state, id) {
        // add 'deleting:true' property to user being deleted
        state.all.items = state.all.items.map(user =>
            user.id === id
                ? { ...user, deleting: true }
                : user
        )
    },
    deleteSuccess(state, id) {
        // remove deleted user from state
        state.all.items = state.all.items.filter(user => user.id !== id)
    },
    Failure(state, error) {
        state.status = {};
    },
    deleteFailure(state, { id, error }) {
        // remove 'deleting:true' property and add 'deleteError:[error]' property to user 
        state.all.items = state.items.map(user => {
            if (user.id === id) {
                // make copy of user without 'deleting:true' property
                const { deleting, ...userCopy } = user;
                // return copy of user with 'deleteError:[error]' property
                return { ...userCopy, deleteError: error };
            }

            return user;
        })
    }
};

export const projects = {
    namespaced: true,
    state,
    actions,
    mutations
};