import { userService } from '../_services';
import { router } from '../_helpers';

const user = JSON.parse(localStorage.getItem('Trophies/user'));
const state = user
    ? { status: { loggedIn: true }, user }
    : { status: {}, user: null };

const actions = {
    login({ dispatch, commit }, { email, password, deviceId }) {
        commit('loginRequest', { email });
    
        userService.login(email, password, deviceId)
            .then(
                user => {
                    commit('loginSuccess', user);
                    router.push('/');
                },
                error => {
                    commit('loginFailure', error);
                    dispatch('alert/error', error, { root: true });
                }
            );
    },
    logout({ commit }) {
        userService.logout();
        commit('logout');
    },
    register({ dispatch, commit }, { email, password } ) {
        userService.register(email, password)
            .then(
                response => {
                    commit('registerSuccess', response);
                    router.push({path: '/register/confirm', 
                                name: 'ConfirmRegistration',
                                props: {items: response}},
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                        dispatch('alert/success', 'Registration pending', { root: true });
                    })
                   },
                error => {
                    commit('registerFailure', error);
                    dispatch('alert/error', error, { root: true });
                } );
                }
            );
            commit('registerRequest', { email, password, response }  );
    },
    confirmRegistration({ dispatch, commit }, { otp, email } ) {
        commit('registerRequest', { otp , email}  );
        userService.confirmRegistration( otp, email )
            .then(
                response => {
                    commit('registerSuccess');
                    router.push({path: '/', 
                                props: {items: response}},
                                    
                   () => {
                    setTimeout(() => {
                        // display success message after route change completes
                        dispatch('alert/success', 'Registration pending', { root: true });
                    })
                   },
                error => {
                    commit('registerFailure', error);
                    dispatch('alert/error', error, { root: true });
                } );
                }
            );        
    }
};

const mutations = {
    loginRequest(state, user) {
        state.status = { loggingIn: true };
        state.user = user;
    },
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = user.email;
    },
    loginFailure(state) {
        state.status = {};
        state.user = null;
    },
    logout(state) {
        state.status = {};
        state.user = null;
    },
    registerRequest(state, user) {
        state.status = { registering: true };
    },
    registerSuccess(state, user) {
        state.status = {};
    },
    registerFailure(state, error) {
        state.status = {};
    }
};

export const account = {
    namespaced: true,
    state,
    actions,
    mutations
};