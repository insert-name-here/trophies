import Vue from 'vue';
import VeeValidate from 'vee-validate';
import Vuetify from 'vuetify';

import { store } from './_store';
import { router } from './_helpers';
import App from './app/App';
import HeaderItems from './components/HeaderItems.vue';
import SidebarHeader from './components/SidebarHeader.vue';
import SidebarItems from './components/SidebarItems.vue';

Vue.use(Vuetify);

new Vue({
    el: '#app',
    router,
    vuetify : new Vuetify(),
    store,
    components: {
    'HeaderItems': HeaderItems,
    'SidebarHeader': SidebarHeader,
    'SidebarItems' : SidebarItems
  },
    render: h => h(App)
});