var path = require('path');
module.exports = {
    configureWebpack: {
      resolve: {
        alias: {
            'home': path.resolve(__dirname, 'src/home'),
        }
      },
    }
  }