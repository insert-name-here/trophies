# Trophies

Contributing to open source projects should earn the developer tokens in the same way as one purchases shares of a company’s stock. If and when the project makes any money, by donation or by sales, contributors holding tokens would be paid dividends based on their proportion of tokens. This concept could also apply to startups that have limited funding or are self-funded and would like to augment what they are paying people with these tokens and should the startup/product be successful then the token can be used to distribute funds fairly.

Ultimately we’d like this to be something that could be integrated into any open source project in order to motivate contributors and turn what are currently acts of altruism into potential long-term investments. The aim of this proof of concept is to build such a tool, while “eating our own dogfood” and using this tool to determine each team member’s share of the winnings!

**NOTE: due to the complexity of blockchain transactions / smart contracts where the tokens are not exchanged, but used to determine proportion, any blockchain development is out of scope of this iteration for the hackathon. The Investec programmable bank card and virtual cards (if available) are perfectly suited for the purposes of a proof-of-concept.**

## Project documentation

All project documentation to be maintained in [this repository's wiki](https://gitlab.com/insert-name-here/trophies/-/wikis/home).

## Project management

While there are multiple repositories involved in this project, we will use this one for project tracking. Please create any tasks or issues using [the boards](https://gitlab.com/insert-name-here/trophies/-/boards), and please ensure that all work done has a ticket assigned to the relevant user to ensure that all tickets are eligible for token reward as soon as the token manager is online.

## Communication

Please reach out to us at #team-hackathon-insert-name on the [Offerzen Community slack](offerzen-community.slack.com) or create issues on the board.

# Trophies Client

This project contains the Trophies web interface.

1. Download or clone the project source code from https://gitlab.com/insert-name-here/trophies.git
	```git clone -b 4-web-portal-mobile-app https://gitlab.com/insert-name-here/trophies.git```
2. Install all required npm packages by running ```npm install``` within the trophies directory.
3. Start the application by running ```npm start``` from the command line in the project root folder, this will automatically launch a browser displaying the application.

# Attributions

[Placeholder Trophies icon by icons8](https://icons8.com/)
